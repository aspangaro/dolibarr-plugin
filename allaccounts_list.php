<?php
/* Copyright (C) 2007-2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *   	\file       myaccount_list.php
 *		\ingroup    doliscan
 *		\brief      List page for myaccount
 */

//if (! defined('NOREQUIREDB'))              define('NOREQUIREDB', '1');					// Do not create database handler $db
//if (! defined('NOREQUIREUSER'))            define('NOREQUIREUSER', '1');					// Do not load object $user
//if (! defined('NOREQUIRESOC'))             define('NOREQUIRESOC', '1');					// Do not load object $mysoc
//if (! defined('NOREQUIRETRAN'))            define('NOREQUIRETRAN', '1');					// Do not load object $langs
//if (! defined('NOSCANGETFORINJECTION'))    define('NOSCANGETFORINJECTION', '1');			// Do not check injection attack on GET parameters
//if (! defined('NOSCANPOSTFORINJECTION'))   define('NOSCANPOSTFORINJECTION', '1');			// Do not check injection attack on POST parameters
//if (! defined('NOCSRFCHECK'))              define('NOCSRFCHECK', '1');					// Do not check CSRF attack (test on referer + on token if option MAIN_SECURITY_CSRF_WITH_TOKEN is on).
//if (! defined('NOTOKENRENEWAL'))           define('NOTOKENRENEWAL', '1');					// Do not roll the Anti CSRF token (used if MAIN_SECURITY_CSRF_WITH_TOKEN is on)
//if (! defined('NOSTYLECHECK'))             define('NOSTYLECHECK', '1');					// Do not check style html tag into posted data
//if (! defined('NOIPCHECK'))                define('NOIPCHECK', '1');						// Do not check IP defined into conf $dolibarr_main_restrict_ip
//if (! defined('NOREQUIREMENU'))            define('NOREQUIREMENU', '1');					// If there is no need to load and show top and left menu
//if (! defined('NOREQUIREHTML'))            define('NOREQUIREHTML', '1');					// If we don't need to load the html.form.class.php
//if (! defined('NOREQUIREAJAX'))            define('NOREQUIREAJAX', '1');       		  	// Do not load ajax.lib.php library
//if (! defined("NOLOGIN"))                  define("NOLOGIN", '1');						// If this page is public (can be called outside logged session)
//if (! defined("MAIN_LANG_DEFAULT"))        define('MAIN_LANG_DEFAULT', 'auto');			// Force lang to a particular value
//if (! defined("MAIN_AUTHENTICATION_MODE")) define('MAIN_AUTHENTICATION_MODE', 'aloginmodule');		// Force authentication handler
//if (! defined("NOREDIRECTBYMAINTOLOGIN"))  define('NOREDIRECTBYMAINTOLOGIN', '1');		// The main.inc.php does not make a redirect if not logged, instead show simple error message
//if (! defined("XFRAMEOPTIONS_ALLOWALL"))   define('XFRAMEOPTIONS_ALLOWALL', '1');			// Do not add the HTTP header 'X-Frame-Options: SAMEORIGIN' but 'X-Frame-Options: ALLOWALL'

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/main.inc.php";
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME'];
$tmp2 = realpath(__FILE__);
$i = strlen($tmp) - 1;
$j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
    $i--;
    $j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1)) . "/main.inc.php")) $res = @include substr($tmp, 0, ($i + 1)) . "/main.inc.php";
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php")) $res = @include dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php";
// Try main.inc.php using relative path
if (!$res && file_exists("../main.inc.php")) $res = @include "../main.inc.php";
if (!$res && file_exists("../../main.inc.php")) $res = @include "../../main.inc.php";
if (!$res && file_exists("../../../main.inc.php")) $res = @include "../../../main.inc.php";
if (!$res) die("Include of main fails");

require_once DOL_DOCUMENT_ROOT . '/core/class/html.formcompany.class.php';
require_once DOL_DOCUMENT_ROOT . '/core/lib/date.lib.php';
require_once DOL_DOCUMENT_ROOT . '/core/lib/company.lib.php';

// load doliscan libraries
require_once __DIR__ . '/class/myaccount.class.php';

// for other modules
//dol_include_once('/othermodule/class/otherobject.class.php');

$permissiontoread = $user->rights->doliscan->allaccounts->read;
$permissiontoadd = $user->rights->doliscan->allaccounts->write; // Used by the include of actions_addupdatedelete.inc.php and actions_lineupdown.inc.php
$permissiontodelete = $user->rights->doliscan->allaccounts->delete || ($permissiontoadd && isset($object->status) && $object->status == $object::STATUS_DRAFT);

// Load translation files required by the page
$langs->loadLangs(array("doliscan@doliscan", "other"));

// Security check
if (empty($conf->doliscan->enabled)) accessforbidden('Module not enabled');

if (!$permissiontoread) accessforbidden();

$socid = 0;
if ($user->socid > 0)    // Protection if external user
{
    //$socid = $user->socid;
    accessforbidden();
}
//$result = restrictedArea($user, 'doliscan', $id, '');
//if (!$permissiontoread) accessforbidden();


/*
 * View
 */

$form = new Form($db);

$now = dol_now();

//$help_url="EN:Module_MyAccount|FR:Module_MyAccount_FR|ES:Módulo_MyAccount";
$help_url = '';
$title = $langs->trans('ListOf', $langs->transnoentitiesnoconv("MyAccounts"));
llxHeader('', $title, $help_url);

print '<h2>Liste des comptes utilisateurs</h2>';

if ($permissiontoadd) {
    if (file_exists('partner.ini')) {
        $partner = parse_ini_file('partner.ini');
    } else {
        $partner = parse_ini_file('partner.ini-dist');
    }

    echo '<div class="warning"><span class="fa fa-warning"> </span> <span class="clear"> ' . $langs->trans("DoliscanSetupInfo1", $partner['tarifs']) . '</span><br />';
    echo '<span class="">' . $langs->trans("DoliscanSetupInfo2", $partner['conditionsgenerales']) . '</span><br />';
    echo '<span class="">' . $langs->trans("DoliscanSetupInfo3", $partner['documentation']) . '</span><br />';
    echo '</div>';
}

$cols = ['id', 'Identifiant Dolibarr', 'Nom', 'Prénom', 'Compte DoliSCAN'];

print '<div class="div-table-responsive">'; // You can use div-table-responsive-no-min if you dont need reserved height for your table
print '<table class="tagtable nobottomiftotal liste' . ($moreforfilter ? " listwithfilterbefore" : "") . '">' . "\n";

print '<tr class="liste_titre">';
foreach ($cols as $c) {
    print '<td class="liste_titre' . ($cssforfield ? ' ' . $cssforfield : '') . '">' . $c . '</td>';
}
print '</tr>';

//tous les utilisateurs locaux
//TODO (attention au multicompany ?)
$sql = "SELECT rowid,login,lastname,firstname FROM " . MAIN_DB_PREFIX . "user WHERE statut='1'";
$resql = $db->query($sql);
if ($resql) {
    while ($objp = $db->fetch_object($resql)) {

        print '<tr class="oddeven">';
        foreach ($objp as $k => $v) {
            print '<td>';
            print $v;
            print '</td>';
        }

        //On cherche son compte DoliSCAN ...
        $sql2 = "SELECT rowid,dsLogin FROM " . MAIN_DB_PREFIX . "doliscan_myaccount WHERE fk_user='" . $objp->rowid . "'";
        $resql2 = $db->query($sql2);
        if ($resql2) {
            $objp2 = $db->fetch_object($resql2);
            $useridto = $objp->rowid;
            $uri = dol_buildpath('/doliscan/myaccount_card.php', 1);
            $back = urlencode(dol_buildpath('/doliscan/allaccounts_list.php', 1));
            if ($objp2->dsLogin) {
                print "<td>" . $objp2->dsLogin;
                if ($permissiontodelete) {
                    print ", <a href=\"" . $uri . "?action=admindelete&useridtodelete=" . $useridto .  "&rowidtodelete=" . $objp2->rowid . "&backtopage=" . $back . "\">cliquez ici pour le supprimer</a>";
                }
                print "</td>";
            } else {
                ///custom/doliscan/myaccount_card.php
                print "<td>Pas encore de compte";
                if ($permissiontoadd) {
                    print ", <a href=\"" . $uri . "?action=admincreate&useridtocreate=" . $useridto . "&backtopage=" . $back . "\">cliquez ici pour le créer</a>";
                }
                print "</td>";
            }
        }
        print '</tr>';
    }
}


print '</table>';
