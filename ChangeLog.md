# CHANGELOG DOLISCAN FOR [DOLIBARR ERP CRM](https://www.dolibarr.org)

## 1.5.1

* Correction affichage des onglets dans l'administration du module

## 1.5

 * Corrige le lancement de la tâche planifiée qui télécharge toutes les notes de frais le 6 du mois pour
   être certain que le serveur DoliSCAN ait eu le temps de générer tous les PDF avant ...

## 1.4

 * Corrige un problème de droits d'accès : ajout des droits pour que les utilisateurs puissent consulter
   et supprimer leurs propres notes de frais
 * Ajoute le numéro de version du plugin dolibarr dans les requêtes réseau pour permettre d'améliorer les
   diagnostics côté serveur doliscan
 * Implémente la possibilité pour l'admin de supprimer les relations doliscan/dolibarr tout comme il peut
   les créer
 * Ajout d'un bouton "sync all" pour synchroniser toutes les notes de frais sur la page d'un utilisateur
   qui a les droits en question (en complément de la tâche planifiée qui fait la même chose)

## 1.3

 * Prise en charge de dolibarr 13

## 1.2

 * Modification de l'affichage de la liste des comptes pour n'avoir
   que les comptes actifs

## 1.1

 * Amélioration des requêtes réseau pour faciliter l'identification et les logs côté serveur DoliSCAN
 * Création d'une page permettant à un responsable RH de créer les comptes des utilisateurs du dolibarr
 * Note pour les revendeurs : Voir le fichier partner.ini pour renseigner des informations personnalisées

## 1.0

Initial version
