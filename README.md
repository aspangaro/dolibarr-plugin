# DOLISCAN POUR [DOLIBARR ERP CRM](https://www.dolibarr.org)

## Fonctionnalités

DoliSCAN est un assistant de saisie dédié aux notes de Frais. Il est composé d'une application à installer sur votre Smartphone et d'un service en ligne lequel est connecté à votre dolibarr par le biais de ce module (plugin).

Ce module est totalement documenté sur le [site doliscan.fr](https://www.doliscan.fr/fr/plugins/dolibarr)

<!--
![Screenshot doliscan](img/screenshot_doliscan.png?raw=true "Doliscan"){imgmd}
-->

## Traductions

L'ensemble DoliSCAN est actuellement uniquement disponible en Français car le moteur de calcul des règles fiscales n'est implémenté que pour les spécificités de la règlementation Française (indemnités kilométriques, remboursement de la TVA variable sur le carburant etc.).

Si vous voulez une version dans votre langue n'hésitez pas à prendre contact avec nous.

## Installation

### À partir d'un fichier ZIP et de l'interface

- Ouvrez votre navigateur et authentifiez vous dans dolibarr avec le compte administrateur, puis allez sur Accueil > Configuration > Modules/Applications > Déployer/Installer un module externe

En cas de problème d'installation vérifiez les points suivants:

- Dans le répertoire d'installation de votre Dolibarr, modifiez le fichier ```htdocs/conf/conf.php``` et vérifiez que les deux lignes suivantes ne sont pas en commentaire:

    ```php
    //$dolibarr_main_url_root_alt ...
    //$dolibarr_main_document_root_alt ...
    ```

- Dé-commentez-les si nécessaire (en supprimant les ```//``` de début de ligne) et affectez les bonnes valeurs correspondantes à votre installation de Dolibarr

    Par exemple :

    - UNIX:
        ```php
        $dolibarr_main_url_root_alt = '/custom';
        $dolibarr_main_document_root_alt = '/var/www/Dolibarr/htdocs/custom';
        ```

    - Windows:
        ```php
        $dolibarr_main_url_root_alt = '/custom';
        $dolibarr_main_document_root_alt = 'C:/My Web Sites/Dolibarr/htdocs/custom';
        ```

### À partir du dépôt de code source GIT:

- Clonez le dépôt dans ```$dolibarr_main_document_root_alt/doliscan```

```sh
cd <dolibarrdir>/htdocs/custom
git clone https://projets.cap-rel.fr/informatique/dolibarr/plugin-doliscan/doliscan-for-dolibarr.git doliscan
```

Note: vérifiez les tags si vous voulez récupérer une version stable particulière ...

### <a name="final_steps"></a>Dernière étape

À partir de votre navigateur web:

  - Connectez vous en tant qu'administrateur à votre Dolibarr
  - Allez sur la page Accueil > Configuration > Modules/Applications
  - Vous devriez voir le module DoliSCAN

## Licence

### Main code

Tout le code est sous licence GPLv3. Regardez le fichier COPYING pour plus d'informations.

### Documentation

Toute la documentation est sous licence CC-BY-SA. Vous la trouverez sur le [site doliscan.fr](https://www.doliscan.fr/fr/plugins/dolibarr)

