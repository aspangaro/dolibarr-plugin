<?php
/* Copyright (C) 2017  Laurent Destailleur <eldy@users.sourceforge.net>
 * Copyright (C) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file        class/mydoliscan.class.php
 * \ingroup     doliscan
 * \brief       This file is a CRUD class file for MyNDF (Create/Read/Update/Delete)
 */

// Put here all includes required by your class file
require_once DOL_DOCUMENT_ROOT . '/core/class/commonobject.class.php';
require_once __DIR__ . '/myndf.class.php';
require_once __DIR__ . '/myaccount.class.php';
require_once __DIR__ . '/../vendor/autoload.php';
dol_include_once('/doliscan/lib/doliscan_myndf.lib.php');
dol_include_once('/doliscan/core/modules/modDoliscan.class.php');

/**
 * Class for MyDoliSCAN
 */
class MyDoliSCAN extends CommonObject
{
    /**
     * @var string ID to identify managed object
     */
    public $element = 'mydoliscan';

    /**
     * @var int  Does this object support multicompany module ?
     * 0=No test on entity, 1=Test with field entity, 'field@table'=Test with link by field@table
     */
    public $ismultientitymanaged = 0;

    /**
     * @var int  Does object support extrafields ? 0=No, 1=Yes
     */
    public $isextrafieldmanaged = 0;

    /**
     * Action executed by scheduler
     * CAN BE A CRON TASK. In such a case, parameters come from the schedule job setup field 'Parameters'
     * Use public function doScheduledJob($param1, $param2, ...) to get parameters
     *
     * @return	int			0 if OK, <>0 if KO (this function is used also by cron so only 0 is OK)
     */
    public function doScheduledJob()
    {
        global $conf, $langs;

        //$conf->global->SYSLOG_FILE = 'DOL_DATA_ROOT/dolibarr_mydedicatedlofile.log';

        $error = 0;
        $this->output = '';
        $this->error = '';

        dol_syslog(__METHOD__, LOG_DEBUG);

        $now = dol_now();

        $this->db->begin();

        // ...

        $this->db->commit();

        return $error;
    }

    /**
     * Sync between DoliSCAN and Dolibarr for all users
     *
     * @return	int			0 if OK, <>0 if KO (this function is used also by cron so only 0 is OK)
     */
    public function syncAllAccounts($force = 0)
    {
        global $conf, $db, $langs;

        //cron lauch every day by default, so please sync only after close date (doliscan close date is 6)
        if (($force == 0) && (date('j') != 6)) {
            return;
        }

        $endpoint = $conf->global->DOLISCAN_MAINSERVER;
        //Pour chaque utilisateur qui a un compte doliscan on lance un sync
        $account = new MyAccount($db);
        $accountRes = $account->fetchAll();
        foreach ($accountRes as $key => $val) {
            $userid = $val->fk_user;
            $dsLogin = $val->dsLogin;
            $dsAPI = $val->dsAPI;

            $u = new User($db);
            $u->fetch($userid);

            $this->syncAccount($u, $endpoint, $dsAPI, $dsLogin);
        }
        return 0;
    }

    public function syncAccount($user, $endpoint, $userAPI, $userLogin)
    {
        global $conf, $db, $langs;
        // print "<pre>";
        // print_r($userAPI);
        // exit;
        $modDoliscan = new modDoliscan($db);

        dol_syslog("(syncAccount) Test if credential to endpoint " . $endpoint . " with userAPI=" . $userAPI . " and $userLogin works ...");

        try {
            $headers = [
                'Accept' => 'application/json',
                'User-Agent' => 'dolibarr/' . $conf->global->MAIN_INFO_SOCIETE_NOM . "(" . $modDoliscan->version . ")",
                'Authorization' => 'Bearer ' . $userAPI,
            ];
            $client = new GuzzleHttp\Client([
                'base_uri' => $endpoint,
                'timeout' => 10,
                'headers' => $headers,
            ]);
            $response = $client->request('GET', 'api/NdeFrais');

            dol_syslog("Request response status code :  " . $response->getStatusCode());
            dol_syslog("Request response data :  " . json_encode($response));
            $data = json_decode($response->getBody());
            dol_syslog("Request response body :  " . json_encode($data));

            if ($response->getStatusCode() == 200) {
                $mesg = '<div class="ok">' . $langs->trans("CheckConnectOK") . '</div>';
                // print "<pre>";
                // print_r($user);
                // exit;

                foreach ($data as $key => $val) {
                    $dsID = $val->id;
                    // print "on a id = $dsID";
                    //On cherche si la NDF existe deja et si c'est le cas on update
                    $n = new MyNDF($db);
                    $n->amount = $val->montant;
                    $n->label = $val->label;
                    $n->fk_user = $user->id;
                    $n->json = json_encode($val); //On sauvegarde le contenu du json
                    $n->description = $langs->trans("ImportFromDoliSCANFor") . " " . $val->label . " (" . $user->firstname . " " . $user->lastname . ")";
                    $n->status = $n::STATUS_DRAFT;
                    $n->import_key = $val->sid;
                    $doublon = $n->fetchAll('', '', 0, 0, array('label' => $val->label, 'fk_user' => $user->id));
                    if (count($doublon) > 0) {
                        dol_syslog("Doublon detected");
                        // $n->update($user);
                    } else {
                        $n->ref = "DOLISCAN-" . substr(dol_string_unaccent($user->firstname), 0, 1) . substr(dol_string_unaccent($user->lastname), 0, 1) . str_replace("-", '', $val->debut);
                        $ndfID = $n->create($user);
                        if ($ndfID) {
                            $ndfOK = new MyNDF($db);
                            if ($ndfOK->fetch($ndfID)) {
                                dol_syslog(" ========================= DOWNLOAD PDF Request 2 for " . $ndfOK->import_key . " ============================= ");

                                $ref = dol_sanitizeFileName($ndfOK->ref);
                                $upload_dir = $conf->doliscan->dir_output . "/myndf/" . $ref;
                                $file_name = ($upload_dir . "/" . $ref . ".pdf");

                                dol_syslog(" download pour $ref : $upload_dir -> $file_name");

                                if (!is_dir($upload_dir)) {
                                    dol_mkdir($upload_dir);
                                }
                                if (is_dir($upload_dir)) {

                                    dol_syslog(" on lance le download vers $file_name ...");

                                    // print "<p>on cherche les détails</p>";
                                    // print_r($n);
                                    //Ajout du document joint ?
                                    $headers2 = [
                                        'Content-Type' => 'application/pdf',
                                        'Cache-Control' => 'no-cache',
                                        'User-Agent' => 'dolibarr/' . $conf->global->MAIN_INFO_SOCIETE_NOM . "(" . $modDoliscan->version . ")",
                                        'Authorization' => 'Bearer ' . $userAPI,
                                    ];
                                    $client2 = new GuzzleHttp\Client([
                                        'base_uri' => $endpoint,
                                        'timeout' => 5000,
                                        'headers' => $headers2,
                                    ]);
                                    $response2 = $client2->request('GET', 'api/NdeFraisPDF/' . $ndfOK->import_key, [
                                        'sink' => $file_name,
                                        // 'debug' => true,
                                    ]);
                                    usleep(500000);

                                    dol_syslog("Request2 response status code :  " . $response2->getStatusCode());
                                    dol_syslog("Request2 response data :  " . json_encode($response2));
                                    $data2 = json_decode($response2->getBody());
                                    dol_syslog("Request2 response body :  " . json_encode($data2));
                                    if ($response2->getStatusCode() == 200) {
                                        $h2 = "";
                                        foreach ($response2->getHeaders() as $name => $values) {
                                            $h2 .= $name . ': ' . implode(', ', $values) . "\r\n";
                                        }
                                        dol_syslog("Request2 response headers :  " . $h2);


                                        //Maintenant on recupere le contenu complet de la NDF
                                        importNDF($ndfOK->id, $user);
                                        usleep(500000);
                                    }
                                    dol_syslog(" ========================= END request 2 for " . $ndfOK->import_key . " ============================= ");
                                }
                            }
                        }
                    }
                }

                // $num = $this->ref;
                // $this->newref = $num;
            } else {
                dol_print_error($db);
            }
        } catch (Exception $e) {
            setEventMessages($e->getMessage(), null, 'errors');
            $action = '';
        }
    }
}
