<?php
/* Copyright (C) 2020 Éric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    doliscan/lib/doliscan.lib.php
 * \ingroup doliscan
 * \brief   Library files with common functions for Doliscan
 */

/**
 * Prepare admin pages header
 *
 * @return array
 */
function doliscanAdminPrepareHead()
{
    global $langs, $conf;

    $langs->loadLangs(array("doliscan@doliscan", 'banks', 'categories', 'accountancy', 'compta'));

    $h = 0;
    $head = array();

    $head[$h][0] = dol_buildpath("/doliscan/admin/setup.php", 1);
    $head[$h][1] = $langs->trans("1Server");
    $head[$h][2] = 'server';
    $h++;
    $head[$h][0] = dol_buildpath("/doliscan/admin/configurationpro.php", 1);
    $head[$h][1] = $langs->trans("2SuppliersLinks");
    $head[$h][2] = 'configurationpro';
    $h++;
    $head[$h][0] = dol_buildpath("/doliscan/admin/configurationperso.php", 1);
    $head[$h][1] = $langs->trans("3FeesType");
    $head[$h][2] = 'configurationperso';
    $h++;
    $head[$h][0] = dol_buildpath("/doliscan/admin/configurationbanque.php", 1);
    $head[$h][1] = $langs->trans("4PaymentsType");
    $head[$h][2] = 'configurationbanque';
    $h++;
    $head[$h][0] = dol_buildpath("/doliscan/admin/about.php", 1);
    $head[$h][1] = $langs->trans("5end");
    $head[$h][2] = 'about';
    $h++;

    // Show more tabs from modules
    // Entries must be declared in modules descriptor with line
    //$this->tabs = array(
    //    'entity:+tabname:Title:@doliscan:/doliscan/mypage.php?id=__ID__'
    //); // to add new tab
    //$this->tabs = array(
    //    'entity:-tabname:Title:@doliscan:/doliscan/mypage.php?id=__ID__'
    //); // to remove a tab
    complete_head_from_modules($conf, $langs, null, $head, $h, 'doliscan');

    return $head;
}

function display_company($socid)
{
    global $db;
    dol_syslog("Display Company id $socid ...");

    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "societe WHERE rowid = " . $socid;

    // print $sql;
    $result = $db->query($sql);
    if ($result) {
        while ($objp = $db->fetch_object($result)) {
            return "<a href=\"" . dol_buildpath("/societe/card.php", 1) . "?socid=$socid" . "\">$objp->nom</a>";
        }
    }
}

function display_frais($id)
{
    dol_syslog("Display Frais $id ...");
    global $db;
    global $langs;

    // Select des infos sur le type fees
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "c_type_fees WHERE id = '" . $id . "'";
    $resql = $db->query($sql);
    $label = "";
    if ($resql) {
        if ($objp_fees = $db->fetch_object($resql)) {
            $label = $langs->trans($objp_fees->code);
            if ($label == "")
                $label = $langs->trans($objp_fees->label);
        }
    }
    dol_syslog(" -> " . json_encode($label));
    return $label;
}

function display_banque_et_paiement($banqueid, $paiementid)
{
    dol_syslog("Display Frais $id ...");
    global $db;
    global $langs;

    $label = "";

    //Moyens de paiements
    $sqlp = "SELECT libelle FROM " . MAIN_DB_PREFIX . "c_paiement WHERE id = '" . $paiementid . "'";
    $resp = $db->query($sqlp);
    if ($resp) {
        if ($objp = $db->fetch_object($resp)) {
            $label .= $langs->trans($objp->libelle);
        }
    }
    // dol_syslog(" -> " . json_encode($label));

    // Comptes bancaires:
    $sqlc = "SELECT label FROM " . MAIN_DB_PREFIX . "bank_account WHERE rowid = '" . $banqueid . "'";
    // return $sql;
    $resc = $db->query($sqlc);
    if ($resc) {
        if ($objc = $db->fetch_object($resc)) {
            $label .= " " . $objc->label;
        }
    }
    dol_syslog(" -> " . json_encode($label));
    return $label;
}
