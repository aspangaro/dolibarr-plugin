<?php
/* Copyright (C) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

require_once DOL_DOCUMENT_ROOT . '/fourn/class/fournisseur.facture.class.php';
require_once DOL_DOCUMENT_ROOT . '/fourn/class/paiementfourn.class.php';
require_once DOL_DOCUMENT_ROOT . '/expensereport/class/expensereport.class.php';
require_once DOL_DOCUMENT_ROOT . '/ecm/class/ecmfiles.class.php';
dol_include_once('/doliscan/core/modules/modDoliscan.class.php');

/**
 * \file    lib/doliscan_myndf.lib.php
 * \ingroup doliscan
 * \brief   Library files with common functions for MyNDF
 */

/**
 * Prepare array of tabs for MyNDF
 *
 * @param    MyNDF    $object        MyNDF
 * @return     array                    Array of tabs
 */
function myndfPrepareHead($object)
{
    global $db, $langs, $conf;

    $langs->load("doliscan@doliscan");

    $h = 0;
    $head = array();

    $head[$h][0] = dol_buildpath("/doliscan/myndf_card.php", 1) . '?id=' . $object->id;
    $head[$h][1] = $langs->trans("Card");
    $head[$h][2] = 'card';
    $h++;

    // if (isset($object->fields['note_public']) || isset($object->fields['note_private']))
    // {
    //     $nbNote = 0;
    //     if (!empty($object->note_private)) $nbNote++;
    //     if (!empty($object->note_public)) $nbNote++;
    //     $head[$h][0] = dol_buildpath('/doliscan/myndf_note.php', 1).'?id='.$object->id;
    //     $head[$h][1] = $langs->trans('Notes');
    //     if ($nbNote > 0) $head[$h][1] .= (empty($conf->global->MAIN_OPTIMIZEFORTEXTBROWSER) ? '<span class="badge marginleftonlyshort">'.$nbNote.'</span>' : '');
    //     $head[$h][2] = 'note';
    //     $h++;
    // }

    require_once DOL_DOCUMENT_ROOT . '/core/lib/files.lib.php';
    require_once DOL_DOCUMENT_ROOT . '/core/class/link.class.php';
    $upload_dir = $conf->doliscan->dir_output . "/myndf/" . dol_sanitizeFileName($object->ref);
    $nbFiles = count(dol_dir_list($upload_dir, 'files', 0, '', '(\.meta|_preview.*\.png)$'));
    $nbLinks = Link::count($db, $object->element, $object->id);
    $head[$h][0] = dol_buildpath("/doliscan/myndf_document.php", 1) . '?id=' . $object->id;
    $head[$h][1] = $langs->trans('Documents');
    if (($nbFiles + $nbLinks) > 0) {
        $head[$h][1] .= '<span class="badge marginleftonlyshort">' . ($nbFiles + $nbLinks) . '</span>';
    }

    $head[$h][2] = 'document';
    $h++;

    // $head[$h][0] = dol_buildpath("/doliscan/myndf_agenda.php", 1).'?id='.$object->id;
    // $head[$h][1] = $langs->trans("Events");
    // $head[$h][2] = 'agenda';
    // $h++;

    // Show more tabs from modules
    // Entries must be declared in modules descriptor with line
    //$this->tabs = array(
    //    'entity:+tabname:Title:@doliscan:/doliscan/mypage.php?id=__ID__'
    //); // to add new tab
    //$this->tabs = array(
    //    'entity:-tabname:Title:@doliscan:/doliscan/mypage.php?id=__ID__'
    //); // to remove a tab
    // complete_head_from_modules($conf, $langs, $object, $head, $h, 'myndf@doliscan');
    // complete_head_from_modules($conf, $langs, $object, $head, $h, 'myndf@doliscan', 'remove');

    return $head;
}

/**
 * fournisseurIdfromSlug transforme le slug doliscan en référence fournisseur dolibarr
 *
 * @param  mixed $slug
 * @return int
 */
function fournisseurIdfromSlug($slug)
{
    global $conf;
    dol_syslog("Recherche du fournisseur dolibarr associé au frais payé pro doliscan $slug ...");
    $dolFraisPro = json_decode($conf->global->DOLISCAN_GLOBAL_FRAISPRO);
    return $dolFraisPro->$slug;
}

/**
 * fraisIdfromSlug transforme le slug doliscan en référence de frais dolibarr
 *
 * @param  mixed $slug
 * @return int
 */
function fraisIdfromSlug($slug)
{
    global $conf;
    dol_syslog("Recherche du frais dolibarr associé au frais payé perso doliscan $slug ...");
    $dolFraisPerso = json_decode($conf->global->DOLISCAN_GLOBAL_FRAISPERSO);
    return $dolFraisPerso->$slug;
}

/**
 * paiementIdFromSlug transforme le slug doliscan en référence de paiement dolibarr
 *
 * @param  mixed $slug
 * @return int
 */
function paiementIdFromSlug($slug)
{
    global $conf;
    $dolBanque = json_decode($conf->global->DOLISCAN_GLOBAL_BANQUE);
    //{"cb-pro":{"idbanque":"1","idpaiement":"6"},"cheque-pro":{"idbanque":"1","idpaiement":"7"},"autre-pro":{"idbanque":"1","idpaiement":"2"},"especes-pro":{"idbanque":"1","idpaiement":"4"}}
    dol_syslog("Recherche du paiement dolibarr associé au paiement doliscan $slug ...");
    return $dolBanque->$slug->idpaiement;
}


/**
 * flatTvaArray : ne garde que les entrées non nulles
 *                note: passe les clés en (string) pour eviter le bug de 5.5 -> 5
 *
 * @param  mixed $l
 * @return array
 */
function flatTvaArray($l)
{
    $t = array();
    if ($l->tvaVal1 > 0) {
        $k = (string)$l->tvaTx1;
        $t[$k] = $l->tvaVal1;
    }
    if ($l->tvaVal2 > 0) {
        $k = (string)$l->tvaTx2;
        $t[$k] = $l->tvaVal2;
    }
    if ($l->tvaVal3 > 0) {
        $k = (string)$l->tvaTx3;
        $t[$k] = $l->tvaVal3;
    }
    if ($l->tvaVal4 > 0) {
        $k = (string)$l->tvaTx4;
        $t[$k] = $l->tvaVal4;
    }

    //Pour avoir le calcul inverse de la tva qui "favorise" les montants les plus importants
    //(ne change pas le montant HT et TTC mais la ventilation dans dolibarr)
    arsort($t);
    return $t;
}

/**
 * create_fact_fournisseur : création de la facture fournisseur
 *
 * @param  mixed $l
 * @return void
 */
function create_fact_fournisseur($l, $user)
{
    global $db, $conf;
    dol_syslog("Création d'une facture fournisseur pour : " . $l->label);
    $r = new stdClass;

    $nberror = 0;
    $facfou = new FactureFournisseur($db);

    $ref = str_replace('-', '', $l->ladate) . ":" . $l->ttc;
    $sql = "SELECT rowid FROM " . MAIN_DB_PREFIX . "facture_fourn WHERE ref_supplier='" . $ref . "' LIMIT 1";
    $resql = $db->query($sql);
    $facid = null;
    if ($resql) {
        $num = $db->num_rows($resql);
        dol_syslog("  Recherche d'un doublon sur ref $ref: $doublon ...");
        if ($num > 0) {
            $obj = $db->fetch_object($resql);
            $facid = $obj->rowid;
            dol_syslog("  doublon trouvé: $facid ...");
            $toto = $facfou->fetch($facid);
            $url = $facfou->getNomUrl(1, '', '', '', '', 0, 0, 0);
            $r->error = "erreur";
            $r->message .= "<li>Une facture fournisseur avec cette référence existe déjà : $l->label du $l->ladate -> $url</li>\n";
        }
    }
    if ($facid == null) {
        $facfou->ref = $ref; //sera remplacee par dolibarr
        $facfou->ref_supplier = $ref; //La ref pour eviter les doublons
        $facfou->socid = fournisseurIdfromSlug($l->type_frais->slug);
        $facfou->libelle = $l->label;
        $facfou->date = strtotime($l->ladate);
        $facfou->date_echeance = strtotime($l->ladate);
        $facfou->note_public = '';
        $facid = $facfou->create($user);

        if ($facid > 0) {
            //A voir si on fait aussi le reglement ou pas...
            // $reglement = new PaiementFourn($db);
            // $reglement->amounts = $l->ttc;
            // $reglement->datepaye = $l->ladate;
            // $reglement->paiementid = paiementIdFromSlug($l->moyen_paiement->slug);
            // $reglement->create($user, $facid, $user->socid);
            // $facfou->set_paid($user);

            //Cas particulier du carburant
            if ($l->type_frais->slug == "carburant") {
                $label = '<strong>Carburant</strong> ' . ndf_label($l) . ' (' . $l->vehicule . ')';
                $amount = ds_nf($l->ttc);
                $qty = '1';
                $price_base = 'HT';
                $tauxtva = '0';
                $remise_percent = 0;
                $fk_product = null;
                $ret = $facfou->addline($label, $amount, $tauxtva, 0, 0, $qty, $fk_product, $remise_percent, '', '', '', 0, $price_base);
                if ($ret < 0) {
                    $nberror++;
                }
            } else {
                $tvaTab = flatTvaArray($l);
                //Pas de TVA
                if (count($tvaTab) == 0) {
                    $label = '<strong>' . ndf_label($l) . '</strong>';
                    $amount = ds_nf($l->ttc);
                    $qty = '1';
                    $price_base = 'HT';
                    $tauxtva = '0';
                    $remise_percent = 0;
                    $fk_product = null;
                    $ret = $facfou->addline($label, $amount, $tauxtva, 0, 0, $qty, $fk_product, $remise_percent, '', '', '', 0, $price_base);
                    if ($ret < 0) {
                        $nberror++;
                    }
                } else {
                    //Un taux de tva
                    if (count($tvaTab) == 1) {
                        foreach ($tvaTab as $tx => $tva) {
                            dol_syslog(" Fact. fournisseur avec un seul taux de TVA : $tx / $tva");
                            $label = '<strong>' . ndf_label($l) . '</strong>';
                            $amount = ds_nf($l->ht);
                            $qty = '1';
                            $price_base = 'HT';
                            $tauxtva = vatrate($tx);
                            $remise_percent = 0;
                            $fk_product = null;
                            $ret = $facfou->addline($label, $amount, $tauxtva, 0, 0, $qty, $fk_product, $remise_percent, '', '', '', 0, $price_base);
                            if ($ret < 0) {
                                $nberror++;
                            }
                        }
                    } else {
                        //plusieurs taux
                        //le pb c'est qu'à force d'utiliser des arrondis les totaux sont faux :-(
                        // on fait donc tourner l'algo pour ensuite aller récupérer les centimes !
                        $nbtaux = 0;
                        $grandTotalTTC = 0;
                        //Si multi taux de tva
                        foreach ($tvaTab as $tx => $tva) {
                            //On a tx=10% et tva=6.67€ il faut remettre le TTC / HT ad hoc
                            $ht = round($tva / $tx * 100, 2);
                            $newtva = round($ht * $tx / 100, 2);
                            $ttc = round($ht + $newtva, 2);
                            $grandTotalTTC += $ttc;
                            $nbtaux++;
                            dol_syslog(" Facture avec Multi-TVA : $tx / $tva => HT=$ht NEWTVA=$newtva || TTC=$ttc");
                            //Dernière chance de corriger la tva
                            if ($nbtaux == count($tvaTab)) {
                                $ttcTicket = $l->ttc;
                                if ($ttcTicket != $grandTotalTTC) {
                                    $correctif = $grandTotalTTC - $ttcTicket;
                                    $ttc -= $correctif;
                                    $ht = round($ttc / (1 + $tx / 100), 2);
                                    $newtva = round($ht * $tx / 100, 2);
                                    dol_syslog(" -> Dernière ligne avec Multi-TVA : HT corrigé=$ht TTC corrigé=$ttc");
                                }
                            }
                            $label = '<strong>' . ndf_label($l) . "</strong>, Total Facture $l->ttx - Partie de la facture avec taux de TVA à $tx%";
                            $amount = ds_nf($ht);
                            $qty = '1';
                            $price_base = 'HT';
                            $tauxtva = vatrate($tx);
                            $remise_percent = 0;
                            $fk_product = null;
                            $ret = $facfou->addline($label, $amount, $tauxtva, 0, 0, $qty, $fk_product, $remise_percent, '', '', '', 0, $price_base);
                            if ($ret < 0) {
                                $nberror++;
                            }
                        }
                    }
                }
            }
            if ($nberror) {
                $db->rollback();
                $r->error = "erreur";
                $r->message = "<li>Erreur de création de la facture fournisseur $l->label du $l->ladate, code d'erreur :" . $facfou->error . "</li>";
            } else {
                $url = $facfou->getNomUrl(1, '', '', '', '', 0, 0, 0);
                $r->error = "erreur";
                $r->message = "<li>Création de la facture fournisseur $l->label du $l->ladate réussie -> $url</li>\n";
                $db->commit();
            }
        } else {
            $db->rollback();
            $r->error = "erreur";
            $r->message = "<li>Erreur de création de la facture fournisseur $l->label du $l->ladate, cette facture semble déjà avoir été importée. Code d'erreur :" . $facfou->error . "</li>";
        }
    }
    //Si pas d'erreur pour la création de la fact. fournisseur on essaye de joindre le justificatif
    if ($nberror == 0) {
        if ($facid && $facfou->fetch($facid)) {
            $ref = dol_sanitizeFileName($facfou->ref);
            $upload_dir = $conf->fournisseur->facture->dir_output . '/' . get_exdir($facfou->id, 2, 0, 0, $facfou, 'invoice_supplier') . $ref;

            if (!is_dir($upload_dir)) dol_mkdir($upload_dir);

            if (is_dir($upload_dir) && ($l->fileName != "")) {
                $uri = "/ldfImages/" . $user->email . "/" . $l->fileName;
                $ext = pathinfo($l->fileName, PATHINFO_EXTENSION);
                //$dest = $upload_dir . "/" . $facfou->ref_supplier . ".$ext";
                $dest = $upload_dir . "/" . str_replace('...', '', $l->fileName);
                // file_put_contents($file_name, file_get_contents($url));
                $r->message .= "<ul><li>On télécharge le document justificatif <b>" . basename($dest) . "</b>  : ";
                $ret = download_fichier($uri, $dest);
                $r->message .= $ret->message;
                $r->error .= $ret->error;
                $r->message .= "</li></ul>";
            }
        }
    }

    return $r;
}

/**
 * add_line_ndf ajoute une ligne sur la note de frais $id
 *
 * @param  mixed $id
 * @param  mixed $l
 * @return void
 */
function add_line_ndf($id, $l, $ndfref, $user)
{
    global $db, $conf;
    $r = new stdClass;
    $down = null;

    $idFichierJustif = null;
    //print "Ajout d'une ligne sur la NDF : " . $l->label . " et justificatif " . $l->fileName . "<br />";
    dol_syslog("Ajout d'une ligne sur la NDF ...");

    //On télécharge le justificatif comme ça on peut avoir son id
    $ref = dol_sanitizeFileName($ndfref);
    $upload_dir = $conf->expensereport->dir_output . '/' . $ref;

    if (!is_dir($upload_dir)) dol_mkdir($upload_dir);

    if (is_dir($upload_dir) && ($l->fileName != "")) {
        $uri = "/ldfImages/" . $user->email . "/" . $l->fileName;
        $ext = pathinfo($l->fileName, PATHINFO_EXTENSION);
        $dest = $upload_dir . "/" . str_replace('...', '', $l->fileName);
        $down = download_fichier($uri, $dest);
        $idFichierJustif = $down->ecmID;
        $r->message .= "<li>" . $down->message . " pour " . $l->fileName . " -> id fichier $idFichierJustif</li>";
    }
    $fraisID = fraisIdfromSlug($l->type_frais->slug);

    $tvaTab = flatTvaArray($l);

    // print(json_encode($l) . "\n");

    //Pas de TVA
    if (count($tvaTab) == 0) {
        dol_syslog(" Ligne sans TVA");
        $newndfline = new ExpenseReportLine($db);
        $newndfline->fk_expensereport = $id;
        $newndfline->fk_c_type_fees = $fraisID;
        $newndfline->fk_project = null;
        $newndfline->fk_soc = $user->socid;
        $newndfline->vatrate = '0';
        $newndfline->vat_src_code = null;
        $newndfline->comments = ndf_label($l);
        $newndfline->qty = 1;
        $newndfline->value_unit = ds_nf($l->ttc);
        $newndfline->total_ht = ds_nf($l->ttc);
        $newndfline->total_ttc = ds_nf($l->ttc);
        $newndfline->total_tva = '0';
        $newndfline->date = $l->ladate;
        $newndfline->rule_warning_message = null;
        $newndfline->fk_c_exp_tax_cat = null;
        $newndfline->fk_ecm_files = null;
        $newndfline->fk_expensereport = $id;
        if ($down) {
            $newndfline->fk_ecm_files = $idFichierJustif;
        }
        $result = $newndfline->insert();
        if ($result < 0) {
            $r->error = "erreur";
            $r->message .= "<li>Erreur d'ajout d'une ligne sur la note de frais : $l->label du $l->ladate, code d'erreur :" . $newndfline->error . "</li>";
        }
    } else {
        //Un seul taux de tva
        if (count($tvaTab) == 1) {
            foreach ($tvaTab as $tx => $tva) {
                dol_syslog(" Ligne avec un taux de TVA : $tx / $tva");
                $newndfline = new ExpenseReportLine($db);
                $newndfline->fk_expensereport = $id;
                $newndfline->fk_c_type_fees = $fraisID;
                $newndfline->fk_project = null;
                $newndfline->fk_soc = $user->socid;
                $newndfline->vat_src_code = null;
                $newndfline->comments = ndf_label($l);
                $newndfline->qty = 1;
                $newndfline->value_unit = ds_nf($l->ttc);
                $newndfline->vatrate = vatrate($tx);
                $newndfline->total_ht = ds_nf($l->ht);
                $newndfline->total_ttc = ds_nf($l->ttc);
                $newndfline->total_tva = ds_nf($tva);
                $newndfline->date = $l->ladate;
                $newndfline->rule_warning_message = null;
                $newndfline->fk_c_exp_tax_cat = null;
                $newndfline->fk_ecm_files = null;
                $newndfline->fk_expensereport = $id;
                $newndfline->fk_ecm_files = $idFichierJustif;
                $result = $newndfline->insert(1, true);
                if ($result < 0) {
                    $r->error = "erreur";
                    $r->message .= "<li>Erreur d'ajout d'une ligne sur la note de frais : $l->label du $l->ladate, code d'erreur :" . $newndfline->error . "</li>";
                }
            }
        } else {
            //plusieurs taux
            //le pb c'est qu'à force d'utiliser des arrondis les totaux sont faux :-(
            // on fait donc tourner l'algo pour ensuite aller récupérer les centimes !
            // attention il est aussi possible d'avoir une note avec une partie sans TVA (ex. taxe de séjour)
            $nbtaux = 0;
            $grandTotalTTC = 0;
            $grandTotalHT = 0;
            //Pour deviner le montant qui n'a pas de TVA on part du TTC et on retranchera la TVA au fur et a mesure
            $devineHT = $l->ttc;
            foreach ($tvaTab as $tx => $tva) {
                //On a tx=10% et tva=6.67€ il faut remettre le TTC / HT ad hoc
                $ht = round($tva / $tx * 100, 2);
                $newtva = round($ht * $tx / 100, 2);
                $ttc = round($ht + $newtva, 2);
                $grandTotalTTC += $ttc;
                $grandTotalHT += $ht;
                $devineHT -= $tva;
                $nbtaux++;
                dol_syslog(" Ligne avec Multi-TVA : $tx / $tva => HT=$ht NEWTVA=$newtva || TTC=$ttc");
                //Dernière chance de corriger la tva
                //Soit l'écart est minime -> erreur d'arrondis et on gere ici
                //Soit écart > 1€ et c'est probablement qu'on a une partie avec zéro de TVA
                if ($nbtaux == count($tvaTab)) {
                    $ttcTicket = $l->ttc;
                    if ($ttcTicket != $grandTotalTTC && (($ttcTicket - $grandTotalTTC) < 1)) {
                        $correctif = $grandTotalTTC - $ttcTicket;
                        $ttc -= $correctif;
                        $ht = round($ttc / (1 + $tx / 100), 2);
                        $newtva = round($ht * $tx / 100, 2);
                        dol_syslog(" -> Dernière ligne avec Multi-TVA : HT corrigé=$ht TTC corrigé=$ttc au lieu de TTC ticket=$ttcTicket");
                    } else {
                        //Il faudra ajouter une derniere ligne pour le montant sans TVA
                        $montantSansTVA = $devineHT - $grandTotalHT;
                        dol_syslog(" -> Il faut ajouter une ligne SANS TVA : grand total HT $grandTotalHT  / grand total TTC $grandTotalTTC | $devineHT");
                        $newndfline = new ExpenseReportLine($db);
                        $newndfline->fk_expensereport = $id;
                        $newndfline->fk_c_type_fees = $fraisID;
                        $newndfline->fk_project = null;
                        $newndfline->fk_soc = $user->socid;
                        $newndfline->vat_src_code = null;
                        $newndfline->comments = ndf_label($l) . " - Total facturette $l->ttc - Partie avec taux de TVA à 0%";
                        $newndfline->qty = 1;
                        $newndfline->value_unit = ds_nf($montantSansTVA);
                        $newndfline->vatrate = vatrate(0);
                        $newndfline->total_ht = ds_nf($montantSansTVA);
                        $newndfline->total_ttc = ds_nf($montantSansTVA);
                        $newndfline->total_tva = ds_nf(0);
                        $newndfline->date = $l->ladate;
                        $newndfline->rule_warning_message = null;
                        $newndfline->fk_c_exp_tax_cat = null;
                        $newndfline->fk_ecm_files = null;
                        $newndfline->fk_expensereport = $id;
                        $newndfline->fk_ecm_files = $idFichierJustif;
                        $result = $newndfline->insert();
                        if ($result < 0) {
                            $r->error = "erreur";
                            $r->message .= "<li>Erreur d'ajout d'une ligne sur la note de frais : $l->label du $l->ladate, code d'erreur :" . $newndfline->error . "</li>";
                        }
                    }
                }
                $newndfline = new ExpenseReportLine($db);
                $newndfline->fk_expensereport = $id;
                $newndfline->fk_c_type_fees = $fraisID;
                $newndfline->fk_project = null;
                $newndfline->fk_soc = $user->socid;
                $newndfline->vat_src_code = null;
                $newndfline->comments = ndf_label($l) . " - Total facturette $l->ttc - Partie avec taux de TVA à $tx%";
                $newndfline->qty = 1;
                $newndfline->value_unit = ds_nf($ttc);
                $newndfline->vatrate = vatrate($tx);
                $newndfline->total_ht = ds_nf($ht);
                $newndfline->total_ttc = ds_nf($ttc);
                $newndfline->total_tva = ds_nf($newtva);
                $newndfline->date = $l->ladate;
                $newndfline->rule_warning_message = null;
                $newndfline->fk_c_exp_tax_cat = null;
                $newndfline->fk_ecm_files = null;
                $newndfline->fk_expensereport = $id;
                $newndfline->fk_ecm_files = $idFichierJustif;
                $result = $newndfline->insert();
                if ($result < 0) {
                    $r->error = "erreur";
                    $r->message .= "<li>Erreur d'ajout d'une ligne sur la note de frais : $l->label du $l->ladate, code d'erreur :" . $newndfline->error . "</li>";
                }
            }
        }
    }
    return $r;
}

//Creation d'un label special pour les lignes de frais
//exemple IK on ajoute départ/arrivée/distance/véhicule
//resto on ajoute les invités etc.
function ndf_label($l)
{
    $r = $l->label;
    if ($l->invites != "") {
        $r .= " - " . $l->invites;
    }
    if ($l->depart != "") {
        $r .= " - " . $l->depart . " -> " . $l->arrivee . " (" . $l->distance . " km)";
    }
    return $r;
}

/**
 * create_ndf création d'une nouvelle note de frais
 *
 * @param  mixed $ndf
 * @return void
 */
function create_ndf($ndf, $user)
{
    global $db, $conf;

    $object = new ExpenseReport($db);

    $sql  = "SELECT rowid FROM " . MAIN_DB_PREFIX . "expensereport WHERE date_debut='" . $ndf->debut . "' AND date_fin='" . $ndf->fin . "'";
    $sql .= " AND fk_user_author='" . $user->id . "' AND entity='" . $conf->entity . "' LIMIT 1";

    $error = 0;
    $r = new stdClass;
    $resql = $db->query($sql);
    $id = null;
    if ($resql) {
        $num = $db->num_rows($resql);
        dol_syslog("  Recherche d'un doublon de NDF pour cet utilisateur debut/fin $ndf->debut/$ndf->fin ...");
        if ($num > 0) {
            dol_syslog("  doublon trouvé ...");
            //On retourne -1 pour eviter de re-ajouter des lignes sur la NDF qui existe déjà
            $id = -1;
            // $obj = $db->fetch_object($resql);
            // $id = $obj->rowid;
            // dol_syslog("  doublon trouvé: $id ...");
            // $r = $object->fetch($id);
            // $url = $object->getNomUrl(1, '', '', '', '', 0, 0, 0);
            // $r = "<li>Une note de frais existe déjà pour cette période -> $url</li>\n";
        } else {
            dol_syslog("  pas de doublon !");
        }
    }
    if ($id == null) {
        $object->date_debut = $ndf->debut;
        $object->date_fin = $ndf->fin;
        $object->fk_user_author = $user->id;
        $object->fk_statut = ExpenseReport::STATUS_DRAFT;
        $object->fk_c_paiement = null;
        $object->fk_user_validator = null;
        $object->note_public = null;
        $object->note_private = null;

        $db->begin();

        $id = $object->create($user);
        if ($id <= 0) {
            $error++;
        }

        if (!$error) {
            $db->commit();
            // Header("Location: ".$_SERVER["PHP_SELF"]."?id=".$id);
            // exit;
        } else {
            setEventMessages($object->error, $object->errors, 'errors');
            $db->rollback();
            $id = -1;
        }
    }
    return $id;
}


/**
 * download_fichier : télécharge le fichier de DoliSCAN -> un fichier local
 *
 * @param  mixed $src
 * @param  mixed $dst
 * @return void
 */
function download_fichier($src, $dst)
{
    global $conf, $db, $user;
    global $dolibarr_main_data_root;
    $endpoint = $conf->global->DOLISCAN_MAINSERVER;
    $account = new MyAccount($db);
    $filter = array('fk_user' => $user->id);
    $accountRes = reset($account->fetchAll('', '', 0, 0, $filter));

    $modDoliscan = new modDoliscan($db);

    $userAPI = $accountRes->dsAPI;
    $userMail = $accountRes->dsLogin;

    $ecm = new EcmFiles($db);
    $r = new stdClass;

    try {
        dol_syslog("Download file from $src to $dst ...");

        $headers = [
            'Content-Type' => 'application/json',
            'Cache-Control' => 'no-cache',
            'User-Agent' => 'dolibarr/' . $conf->global->MAIN_INFO_SOCIETE_NOM . "(" . $modDoliscan->version . ")",
            'Authorization' => 'Bearer ' . $userAPI,
            'DocumentFormat' => 'signedPDF',
        ];
        $client = new GuzzleHttp\Client([
            'base_uri' => $endpoint,
            'timeout' => 5000,
            'headers' => $headers,
        ]);
        $response = $client->request('GET', "/api" . $src, [
            // 'sink' => "$dst",
            // 'email' => "$userMail",
            // 'debug' => true,
        ]);
        //Avoid too many requests (api flood)
        usleep(500000);

        dol_syslog("Download response status code :  " . $response->getStatusCode());
        dol_syslog("Download response headers :  " . json_encode($response->getHeaders()));
        $data = json_decode($response->getBody());
        // dol_syslog("Download response :  " . json_encode($data));
        if ($response->getStatusCode() == 200) {
            $filename = str_replace('...', '', basename($data->filename));
            $dst = dirname($dst) . "/" . $filename;
            $ecm->label = $filename;
            $ecm->entity = $conf->entity;
            $ecm->filename = $filename; //dolibarr n'aime pas avoir "..." dans le nom du fichier
            $ecm->filepath = str_replace($dolibarr_main_data_root . "/", '', dirname($dst)); //Il faut virer le /var/www/dolibarr-data/
            $ecm->fk_user_c = $user->id;
            $r->ecmID = $ecm->create($user);
            dol_syslog("ECM CREATE  :" . $r->ecmID);
            if ($ecm->errors) {
                dol_syslog(implode(',', $ecm->errors));
            }

            $fileContent = $data->fileContent;
            file_put_contents($dst, base64_decode($fileContent));
            if (file_exists($dst)) {
                $r->message .= "Téléchargement réussi.";
                dol_syslog("Download OK");
            }
        } else {
            dol_syslog("Download ERR");
            $r->message .= "<b>Erreur de téléchargement du document</b>";
        }
        // dol_syslog(" ========================= END request 2 for " . $ndfOK->import_key . " ============================= ");
    } catch (Exception $e) {
        setEventMessages($e->getMessage(), null, 'errors');
        $action = '';
    }

    return $r;
}

function ds_nf($number)
{
    return $number;
    //return number_format($number, 0, ',', '');
}

/**
 * Import NDF from DoliSCAN to Dolibarr database
 *
 * @param  int  $id        ID of NDF to import
 * @param  User $user      User that creates
 * @return int             <0 if KO, Id of created object if OK
 */
//Importe la note de frais ref id
function importNDF($id, $user)
{
    global $conf, $db, $langs;
    global $dolibarr_main_data_root;

    require_once DOL_DOCUMENT_ROOT . '/core/lib/files.lib.php';
    require_once DOL_DOCUMENT_ROOT . '/core/class/link.class.php';
    require_once DOL_DOCUMENT_ROOT . '/expensereport/class/expensereport.class.php';

    $modDoliscan = new modDoliscan($db);

    $endpoint = $conf->global->DOLISCAN_MAINSERVER;
    $account = new MyAccount($db);

    $myndf = new MyNDF($db);
    $myndf->fetch($id);

    //comme on peut eventuellement importer la ndf d'un autre utilisateur l'appel de la fonction passe le $user ad hoc
    $filter = array('fk_user' => $user->id);
    $accountRes = reset($account->fetchAll('', '', 0, 0, $filter));

    $userLogin = $accountRes->dsLogin;
    $userAPI = $accountRes->dsAPI;

    $msgAndError = new stdClass;
    $errorNDF = ""; //Specifique pour la NDF pour differencier des fact fourn

    //-1 si doublon par exemple
    $expenseRid = create_ndf(json_decode($myndf->json), $user);
    $ndfRef = null;
    if ($expenseRid > 0) {
        $ndfJSON = json_decode($myndf->json);
        $ndfDbarr = new ExpenseReport($db);
        $objndfDbarr = $ndfDbarr->fetch($expenseRid);
        $ndfRef = $ndfDbarr->ref;

        //On essaye de joindre le fichier NDF comme justificatif global
        $ref = dol_sanitizeFileName($ndfRef);
        $upload_dir = $conf->expensereport->dir_output . '/' . $ref;
        $file_name = ($upload_dir . "/" . $ref . "-DOLISCAN.pdf");

        if (!is_dir($upload_dir)) dol_mkdir($upload_dir);

        try {
            dol_syslog(" on lance le download vers $file_name ...");
            $msgAndError->message .= "<li>Téléchargement du justificatif " . $ndfJSON->sid . " vers  $ref-DOLISCAN.pdf ...</li>";

            // print "<p>on cherche les détails</p>";
            // print_r($n);
            //Ajout du document joint ?
            $headers2 = [
                'Content-Type' => 'application/pdf',
                'Cache-Control' => 'no-cache',
                'User-Agent' => 'dolibarr/' . $conf->global->MAIN_INFO_SOCIETE_NOM . "(" . $modDoliscan->version . ")",
                'Authorization' => 'Bearer ' . $userAPI,
            ];
            $client2 = new GuzzleHttp\Client([
                'base_uri' => $endpoint,
                'timeout' => 5000,
                'headers' => $headers2,
            ]);
            $response2 = $client2->request('GET', 'api/NdeFraisPDF/' . $ndfJSON->sid, [
                'sink' => $file_name,
                // 'debug' => true,
            ]);
            //Avoid too many requests (api flood, wait for 500msec)
            usleep(500000);
            dol_syslog("Request2 response status code :  " . $response2->getStatusCode());
            dol_syslog(" ========================= END request 2 for " . $ndfOK->import_key . " ============================= ");
        } catch (Exception $e) {
            setEventMessages($e->getMessage(), null, 'errors');
            $action = '';
        }
    }

    try {
        dol_syslog("Download NDF Details from DoliSCAN ... ($applicationKey)");

        $headers = [
            'Accept' => 'application/json',
            'User-Agent' => 'dolibarr/' . $conf->global->MAIN_INFO_SOCIETE_NOM . "(" . $modDoliscan->version . ")",
            'Authorization' => 'Bearer ' . $userAPI,
        ];

        $client = new GuzzleHttp\Client([
            'base_uri' => $endpoint,
            'timeout' => 300,
            'headers' => $headers,
            'email' => $userLogin,
        ]);
        //attention on utilise fakerid côté doliscan -> utiliser import_key !
        $response = $client->request('GET', 'api/NdeFraisDetails/' . $myndf->import_key, []);
        //Avoid too many requests (api flood, wait for 500msec)
        usleep(500000);

        dol_syslog("Request response status code :  " . $response->getStatusCode());
        $data = json_decode($response->getBody());
        // dol_syslog("Request response body :  " . json_encode($data));

        if ($response->getStatusCode() == 200) {
            $mesg = '<div class="ok">' . $langs->trans("GetTypeFraisProOK") . '</div>';
            foreach ($data as $ldeFrais) {
                //Si c'est un frais payé pro -> facture fournisseur
                if ($ldeFrais->moyen_paiement->is_pro == 1) {
                    $ret = create_fact_fournisseur($ldeFrais, $user);
                    $msgAndError->message .= $ret->message;
                    $msgAndError->error .= $ret->error;
                } else {
                    $fraisID = fraisIdfromSlug($ldeFrais->type_frais->slug);
                    //Si jamais on a un cas particulier pas glop ... du genre carburant payé perso -> facture fournisseur
                    if ($fraisID == "") {
                        $ret = create_fact_fournisseur($ldeFrais, $user);
                        $msgAndError->message .= $ret->message;
                        $msgAndError->error .= $ret->error;
                    } else {
                        if ($expenseRid > 0) {
                            $ret = add_line_ndf($expenseRid, $ldeFrais, $ndfRef, $user);
                            $msgAndError->message .= $ret->message;
                            $msgAndError->error .= $ret->error;
                            $errorNDF .= $ret->error;
                        }
                    }
                }
            }
        } else {
            dol_print_error($db);
        }
    } catch (Exception $e) {
        setEventMessages($e->getMessage(), null, 'errors');
        $action = '';
    }

    if ($msgAndError->error == "") {
        $message = "<li>Import complet sans erreur.</li>";
    }
    if ($expenseRid > 0 && $errorNDF == "") {
        $oburl = new ExpenseReport($db);
        $r = $oburl->fetch($expenseRid);
        $url = $oburl->getNomUrl(1, '', '', '', '', 0, 0, 0);
        $message .= "<li>Note de frais importée -> $url</li>";
        //TODO passer la DoliNDF a validated -> pour que le message affiche maintenant 'déjà importée'
        //$result = $oburl->setValidate($user);
        $result = $myndf->validate($user);
    }
    //On cloture (au cas ou)
    if ($expenseRid == -1) {
        $result = $myndf->validate($user);
    }

    $html = '<div class="fichecenter">';
    $html .= '<div class="underbanner clearboth"></div>';
    $html .= "<p>Résultat de l'import de la note de frais dans Dolibarr:</p>";
    $html .= "<ul>";
    $html .= $message;
    $html .= $msgAndError->message;
    $html .= "</ul>";
    $html .= '</div>';

    return $html;
}
